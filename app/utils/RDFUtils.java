package utils;

import uk.ac.mimas.names.databasemanager.*;
import uk.ac.mimas.names.databasemanager.types.*;

import uk.ac.mimas.names.disambiguator.types.*;
public class RDFUtils{
	/*
	This should all be transferred to a view really..
	*/

	public static String rdfHeader(){
		return"<?xml version=\"1.0\"?><rdf:RDF xmlns:rdf=\"http://www.w3.org/1999/02/22-rdf-syntax-ns#\" xmlns:rdfs=\"http://www.w3.org/2000/01/rdf-schema#\" xmlns:foaf=\"http://xmlns.com/foaf/0.1/\" xmlns:dc=\"http://purl.org/dc/elements/1.1/\">";
	}

	public static String rdfFooter(){
		return "</rdf:RDF>";
	}
    public static String rdfRecords(Response response){
		
    
    	String responseRDF = "";
    	for(NamesRecord namesRecord : response.getResults().getRecords()){
    		responseRDF += rdfRecord(namesRecord) ;
    	}
    	return responseRDF;

    }
	
	public static String rdfRecord(NamesRecord record){
    	String responseRDF = "";
    	responseRDF += "<foaf:Person rdf:about=\"http://names.mimas.ac.uk/blah/"+record.getNamesID()+".rdf#Person\">";
    	for(NormalisedName normalisedName : record.getNormalisedNames()){
    		responseRDF += "<foaf:name>"+normalisedName.getName()+"</foaf:name>";
    	}
        if(record.getNormalisedCollaborations().size() > 0){
            responseRDF +="<foaf:knows>";
            for(NormalisedCollaboration normalisedCollaboration : record.getNormalisedCollaborations()){
                responseRDF += "<foaf:Person xmlns:rdfs=\"http://www.w3.org/2000/01/rdf-schema#\">";
                responseRDF += "<foaf:name>"+normalisedCollaboration.getCollaborationName()+"</foaf:name>";
                responseRDF += "<rdfs:seeAlso xmlns:rdfs=\"http://www.w3.org/2000/01/rdf-schema#\" rdf:resource=\"http://names.mimas.ac.uk/individual/"+normalisedCollaboration.getCollaborationNamesID()+"\"/>";
                responseRDF +="</foaf:Person>";
            }
            responseRDF +="</foaf:knows>";
        }
    	responseRDF += "</foaf:Person>";
    	return responseRDF;
    }


}