package utils;
import play.libs.Json;
import org.codehaus.jackson.node.ObjectNode;
import org.codehaus.jackson.node.ArrayNode;
import org.codehaus.jackson.JsonNode;
import uk.ac.mimas.names.databasemanager.*;
import uk.ac.mimas.names.databasemanager.types.*;

import uk.ac.mimas.names.disambiguator.types.*;
public class JsonUtils{


    public static ObjectNode jsonRecords(Response response){
		ObjectNode result = Json.newObject();
    	result.put("total",response.getResults().getCount());
    	
    	
    	ArrayNode records = result.putArray("results");
 
	    for(NamesRecord namesRecord : response.getResults().getRecords()) {
	     
	      records.add(jsonRecord(namesRecord));
	    }


    	
    
    	return result;

    }
	
	public static ObjectNode jsonRecord(NamesRecord record){
    	ObjectNode result = Json.newObject();
    	result.put("names_id","http://names.mimas.ac.uk/blah/"+record.getNamesID());
    	result.put("type",record.getType());
    	result.put("date_created",record.getDateCreated().toString());
    	result.put("date_modified",record.getDateModified().toString());

    	if(record.getNormalisedNames().size() > 0){
    		ArrayNode names = result.putArray("names");
 
		    for(NormalisedName normalisedName : record.getNormalisedNames()) {
		      ObjectNode name = Json.newObject();
		      name.put("name", normalisedName.getName());
		      name.put("used_from", normalisedName.getUsedFrom().toString());
		      name.put("used_until", normalisedName.getUsedUntil().toString());
		      name.put("source_name", normalisedName.getSourceName());
		      name.put("source_id", normalisedName.getSourceID());
		      name.put("source_url", normalisedName.getSourceURL());
		      name.put("match_score", normalisedName.getMatchScore());
		      names.add(name);
		    }
	    }

	    if(record.getNormalisedIdentifiers().size() > 0){
	    	ArrayNode identifiers = result.putArray("identifiers");

		    for(NormalisedIdentifier normalisedIdentifier : record.getNormalisedIdentifiers()) {
		      ObjectNode identifier = Json.newObject();
		      identifier.put("identifier", normalisedIdentifier.getIdentifier());
		      identifier.put("basis_for", normalisedIdentifier.getBasisFor());
		      identifier.put("source_name", normalisedIdentifier.getSourceName());
		      identifier.put("source_id", normalisedIdentifier.getSourceID());
		      identifier.put("source_url", normalisedIdentifier.getSourceURL());
		      identifier.put("match_score", normalisedIdentifier.getMatchScore());
		      identifiers.add(identifier);
		    }
		}

	    if(record.getNormalisedAffiliations().size() > 0){
	    	ArrayNode affiliations = result.putArray("affiliations");

		    for(NormalisedAffiliation normalisedAffiliation : record.getNormalisedAffiliations()) {
		      ObjectNode affiliation = Json.newObject();
		      affiliation.put("affiliation_name", normalisedAffiliation.getAffiliationName());
		      affiliation.put("affiliation_names_id", normalisedAffiliation.getAffiliationNamesID());
		      affiliation.put("source_name", normalisedAffiliation.getSourceName());
		      affiliation.put("source_id", normalisedAffiliation.getSourceID());
		      affiliation.put("source_url", normalisedAffiliation.getSourceURL());
		      affiliation.put("match_score", normalisedAffiliation.getMatchScore());
		      affiliations.add(affiliation);
		    }
		}

	    if(record.getNormalisedFieldsOfActivity().size() > 0){
	    	ArrayNode fieldsOfActivity = result.putArray("fields_of_activity");

		    for(NormalisedFieldOfActivity normalisedFieldOfActivity : record.getNormalisedFieldsOfActivity()) {
		      ObjectNode fieldOfActivity = Json.newObject();
		      fieldOfActivity.put("field_of_activity", normalisedFieldOfActivity.getFieldOfActivity());
		     
		      fieldOfActivity.put("source_name", normalisedFieldOfActivity.getSourceName());
		      fieldOfActivity.put("source_id", normalisedFieldOfActivity.getSourceID());
		      fieldOfActivity.put("source_url", normalisedFieldOfActivity.getSourceURL());
		      fieldOfActivity.put("match_score", normalisedFieldOfActivity.getMatchScore());
		      fieldsOfActivity.add(fieldOfActivity);
		    }
		}

	    if(record.getNormalisedResultPublications().size() > 0){
	    	ArrayNode resultPublications = result.putArray("result_publications");
	    	
		    for(NormalisedResultPublication normalisedResultPublication : record.getNormalisedResultPublications()) {
		      ObjectNode resultPublication = Json.newObject();
		      resultPublication.put("title", normalisedResultPublication.getTitle());
		      resultPublication.put("number", normalisedResultPublication.getNumber());
		      resultPublication.put("volume", normalisedResultPublication.getVolume());
		      resultPublication.put("edition", normalisedResultPublication.getEdition());
		      resultPublication.put("series", normalisedResultPublication.getSeries());
		      resultPublication.put("issue", normalisedResultPublication.getIssue());
		      resultPublication.put("date_of_publication", normalisedResultPublication.getDateOfPublication());
			  resultPublication.put("abstract", normalisedResultPublication.getAbs());
		      resultPublication.put("source_name", normalisedResultPublication.getSourceName());
		      resultPublication.put("source_id", normalisedResultPublication.getSourceID());
		      resultPublication.put("source_url", normalisedResultPublication.getSourceURL());
		      resultPublication.put("match_score", normalisedResultPublication.getMatchScore());
		      resultPublications.add(resultPublication);
		    }
    	}

    	 if(record.getNormalisedCollaborations().size() > 0){
	    	ArrayNode collaborations = result.putArray("collaborations");

		    for(NormalisedCollaboration normalisedCollaboration : record.getNormalisedCollaborations()) {
		      ObjectNode collaboration = Json.newObject();
		      collaboration.put("collaboration_name", normalisedCollaboration.getCollaborationName());
		      collaboration.put("collaboration_names_id", normalisedCollaboration.getCollaborationNamesID());
		      collaboration.put("source_name", normalisedCollaboration.getSourceName());
		      collaboration.put("source_id", normalisedCollaboration.getSourceID());
		      collaboration.put("source_url", normalisedCollaboration.getSourceURL());
		      collaboration.put("match_score", normalisedCollaboration.getMatchScore());
		      collaborations.add(collaboration);
		    }
		}
    
    	return result;
    }
}