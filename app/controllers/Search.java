package controllers;

import play.*;
import play.mvc.*;
import utils.JsonUtils;
import utils.RDFUtils;

import views.html.*;
import uk.ac.mimas.names.databasemanager.*;
import uk.ac.mimas.names.disambiguator.types.*;


public class Search extends Controller {


  
    public static Result search(String name, 
    						   String affiliation, 
    						   String fieldofactivity, 
    						   String identifier, Integer start) {

		
    	if(name == null && affiliation == null && fieldofactivity == null & identifier == null)
        	return badRequest("invalid search parameters");
       		
       	Query query = new Query();
       	if(name != null)
			query.setNames(name);
		if(affiliation != null)
			query.setAffiliations(affiliation);
		if(fieldofactivity != null)
			query.setFieldsOfActivity(fieldofactivity);
		if(identifier != null)
			query.setIdentifiers(identifier);
		query.setStart(start);
		query.setGetCount(true);
		query.setOrderBy(Query.ORDER_BY_NAME);


		if(query.validQuery()){

			DatabaseManager dm = new DatabaseManager(Play.application().configuration().getString("db.default.driver"),
													 Play.application().configuration().getString("db.default.url"),
													 Play.application().configuration().getString("db.default.user"),
													 Play.application().configuration().getString("db.default.password"));
			Response response;
			try{
				dm.connect();
				response = dm.find(query);
				dm.close();
				if (request().accepts("text/html")){
					return ok(search_results.render(response,query));
				}
				else if(request().accepts("application/json")){
					return ok(JsonUtils.jsonRecords(response));

				}
				else  if(request().accepts("application/rdf+xml")){
					response().setContentType("application/rdf+xml");
					return ok(RDFUtils.rdfHeader()+RDFUtils.rdfRecords(response)+RDFUtils.rdfFooter());

				}
				else{
					return status(406, "Request content-type not supported");
				}
			}
			catch(Exception e){
				Logger.error("Error with DB " + e.toString());
				return internalServerError();
			}
		}
		else{
			return badRequest();
		}
	
     
    }


    
  
}
