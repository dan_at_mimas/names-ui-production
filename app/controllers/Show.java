package controllers;


import play.*;
import play.mvc.*;

import utils.JsonUtils;
import utils.RDFUtils;
import views.html.*;

import uk.ac.mimas.names.databasemanager.*;
import uk.ac.mimas.names.databasemanager.types.*;

import uk.ac.mimas.names.disambiguator.types.*;



public class Show extends Controller {
	


	public static Result show(Long id, String type){

		try{
			short parsedType = type.equalsIgnoreCase("individual") ? NamesRecord.INDIVIDUAL : type.equalsIgnoreCase("institution") ? NamesRecord.INSTITUTION : NamesRecord.UNKNOWN;
			if(parsedType == NamesRecord.UNKNOWN)
				return badRequest("Invalid entity type '"+type+"'");
			DatabaseManager dm = new DatabaseManager(Play.application().configuration().getString("db.default.driver"),
													 Play.application().configuration().getString("db.default.url"),
													 Play.application().configuration().getString("db.default.user"),
													 Play.application().configuration().getString("db.default.password"));
			Response response;

			dm.connect();
			response = dm.get(id.intValue(), parsedType);
			Logger.debug(response.getMessage());
			dm.close();
			if(response.getResults().getCount() <= 0)
				return notFound("Record not found");
			else{
				if(response.getResults().first().getRedirect() > 0){
					return redirect("/"+type+"/"+response.getResults().first().getRedirect());
				}
				if (request().accepts("text/html")){
					return ok(show.render(response.getResults().first()));
				}
				else  if(request().accepts("application/json")){
					return ok(JsonUtils.jsonRecord(response.getResults().first()));

				}
				else  if(request().accepts("application/rdf+xml")){
					response().setContentType("application/rdf+xml");
					return ok(RDFUtils.rdfHeader()+RDFUtils.rdfRecord(response.getResults().first())+RDFUtils.rdfFooter());

				}
				else{
					return status(406, "Request content-type not supported");
				}
			}
		}
		catch(Exception e){
			Logger.error("Error retrieving record " + e.toString());
			return internalServerError();
		}

	}



}