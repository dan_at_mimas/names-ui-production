package controllers;

import play.*;
import play.mvc.*;

import views.html.*;

public class SearchAdvanced extends Controller {
  
    public static Result show() {
        return ok(search_advanced.render());
    }

}