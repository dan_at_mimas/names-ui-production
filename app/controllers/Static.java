package controllers;

import play.*;
import play.mvc.*;

import views.html.*;

public class Static extends Controller {
  
    public static Result showHelp() {
        return ok(help.render());
    }

 	public static Result showAPI() {
        return ok(api.render());
    }

    public static Result showAbout(){
    	return ok(about.render());
    }

    public static Result showPrivacyAndCookies(){
    	return ok(privacy_and_cookies.render());
    }
}